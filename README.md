# NPM cache run on docker + fig

Based on [sinopia](https://github.com/keyvanfatehi/docker-sinopia)

## how to run?

```
#!shell
$ git clone https://bitbucket.org/jagad/npm-local-cache
$ fig up
```
*note: it will build and run the automagically, take times to pull dependancy images if not locally available*

## Setting Registry

```
#!shell
npm set registry http://<docker_host>:4873/
```

## Deployed example

```
#!shell
npm set registry http://udock.cloudapp.net:4873/
```


